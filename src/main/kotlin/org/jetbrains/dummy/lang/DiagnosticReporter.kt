package org.jetbrains.dummy.lang

import java.io.OutputStream
import java.io.PrintStream

class DiagnosticReporter(
    outputStream: OutputStream
) {
    private val outputStream = PrintStream(outputStream)

    fun report(inspectionError: InspectionError) {
        inspectionError.printError(outputStream)
    }
}