package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.tree.Element
import java.io.PrintStream

class InspectionError(
    private val errors: List<Pair<Element, String>>
) {
    fun printError(out: PrintStream) {
        for (error in errors) {
            out.println("ERROR: line ${error.first.line}: ${error.second}")
        }
    }
}