package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.inspections.TypeInspections
import org.jetbrains.dummy.lang.tree.File

class TypesChecker(private val reporter: DiagnosticReporter): AbstractChecker() {
    override fun inspect(file: File) {
        val typeInspectionVisitor = TypeInspections()
        val (inspections, _) = typeInspectionVisitor.visitFile(file, Pair(null, emptyList()))
        reportAccessBeforeInitialization(reporter, inspections)
    }
}