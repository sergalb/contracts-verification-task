package org.jetbrains.dummy.lang

import org.jetbrains.dummy.lang.inspections.InitializeInspectionVisitor
import org.jetbrains.dummy.lang.tree.File

class VariableInitializationChecker(private val reporter: DiagnosticReporter) : AbstractChecker() {
    override fun inspect(file: File) {
        val initializeInspectionVisitor = InitializeInspectionVisitor()
        val (inspectionError, _) = initializeInspectionVisitor.visitFile(file, Pair(0, emptyList()))
        reportAccessBeforeInitialization(reporter, inspectionError)
    }

    // Use this method for reporting errors


}