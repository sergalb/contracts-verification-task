package org.jetbrains.dummy.lang.inspections

import org.jetbrains.dummy.lang.tree.DummyLangVisitor
import org.jetbrains.dummy.lang.tree.Element
import org.jetbrains.dummy.lang.tree.VariableAccess

class ExpressionVariablesCollector: DummyLangVisitor<Set<VariableAccess>, Void?>() {
    private val usedVariables: MutableSet<VariableAccess> = HashSet()
    override fun visitElement(element: Element, data: Void?): Set<VariableAccess> {
        element.acceptChildren(this, data)
        return usedVariables
    }

    override fun visitVariableAccess(variableAccess: VariableAccess, data: Void?): Set<VariableAccess> {
        usedVariables.add(variableAccess)
        return usedVariables
    }
}