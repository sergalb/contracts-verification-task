package org.jetbrains.dummy.lang.inspections

import org.jetbrains.dummy.lang.InspectionError
import org.jetbrains.dummy.lang.tree.*

class InitializeInspectionVisitor :
    DummyLangVisitor<Pair<InspectionError, Set<String>>, Pair<Int, List<Map<String, Boolean>>>>() {
    private val nonInitializedVariables: MutableList<Pair<Element, String>> = ArrayList()

    private fun generateNonInitializedMessage(variableName: String): String {
        return "Variable '$variableName' is accessed before initialization"
    }

    override fun visitElement(
        element: Element,
        data: Pair<Int, List<Map<String, Boolean>>>
    ): Pair<InspectionError, Set<String>> {
        element.acceptChildren(this, data)
        return Pair(
            InspectionError(nonInitializedVariables),
            emptySet()
        )
    }

    override fun visitFile(
        file: File,
        data: Pair<Int, List<Map<String, Boolean>>>
    ): Pair<InspectionError, Set<String>> {
        for (function in file.functions) {
            visitFunctionDeclaration(
                function,
                Pair(1, listOf(function.parameters.associateWith { true }))
            )
        }
        return Pair(InspectionError(nonInitializedVariables), emptySet())
    }

    override fun visitBlock(
        block: Block,
        data: Pair<Int, List<Map<String, Boolean>>>
    ): Pair<InspectionError, Set<String>> {
        val localVariables: MutableMap<String, Boolean> = HashMap()
        val definedVariables = data.second.asSequence().plusElement(localVariables).toList()
        val depth = data.first
        val assignedVariablesInCurBlock: MutableSet<String> = HashSet()
        for (statement in block.statements) {
            when (statement) {
                is Assignment -> processAssigment(
                    statement,
                    localVariables,
                    definedVariables,
                    assignedVariablesInCurBlock
                )

                is VariableDeclaration -> {
                    if (statement.initializer == null) {
                        localVariables[statement.name] = false
                    } else {
                        updateInspectionErrorsByExpression(definedVariables, statement.initializer)
                        localVariables[statement.name] = true
                    }
                }

                is Expression -> updateInspectionErrorsByExpression(definedVariables, statement)

                is ReturnStatement -> if (statement.result !== null) updateInspectionErrorsByExpression(
                    definedVariables,
                    statement.result
                )

                is IfStatement -> processIf(
                    statement,
                    localVariables,
                    definedVariables,
                    assignedVariablesInCurBlock,
                    depth
                )
            }
        }
        return Pair(InspectionError(nonInitializedVariables), assignedVariablesInCurBlock)
    }

    private fun processIf(
        statement: IfStatement,
        localVariables: MutableMap<String, Boolean>,
        definedVariables: List<Map<String, Boolean>>,
        assignedVariablesInCurBlock: MutableSet<String>,
        depth: Int
    ) {
        updateInspectionErrorsByExpression(definedVariables, statement.condition)
        val (_, assignedInThen) = visitBlock(
            statement.thenBlock,
            Pair(depth + 1, definedVariables)
        )
        if (statement.elseBlock != null) {
            val (_, definedInElse) = visitBlock(
                statement.elseBlock,
                Pair(depth + 1, definedVariables)
            )
            val assignedInBoth = assignedInThen.asSequence().filter { definedInElse.contains(it) }

            val localAndParentsVariables = assignedInBoth.groupBy { localVariables.containsKey(it) }
            localAndParentsVariables[true]?.asSequence()?.forEach { localVariables[it] = true }
            localAndParentsVariables[false]?.let { assignedVariablesInCurBlock.addAll(it) }
        }
    }

    private fun processAssigment(
        statement: Assignment,
        localVariables: MutableMap<String, Boolean>,
        definedVariables: List<Map<String, Boolean>>,
        assignedVariablesInCurBlock: MutableSet<String>
    ) {
        val usedVariables = collectVariableInExpression(statement.rhs)
        updateInspectionErrorsByExpressionVariables(definedVariables, usedVariables)
        if (localVariables.containsKey(statement.variable)) {
            localVariables[statement.variable] = true
        } else {
            var variableIsKnown = false
            for (level in definedVariables.asReversed()) {
                if (level.containsKey(statement.variable)) {
                    variableIsKnown = true
                    break
                }
            }
            if (!variableIsKnown) {
                nonInitializedVariables.add(
                    Pair(
                        statement,
                        generateNonInitializedMessage(statement.variable)
                    )
                )
            } else {
                assignedVariablesInCurBlock.add(statement.variable)
            }
        }
    }

    private fun collectVariableInExpression(expression: Expression): Set<VariableAccess> {
        val variablesCollector = ExpressionVariablesCollector()
        return variablesCollector.visitElement(expression, null)
    }

    private fun updateInspectionErrorsByExpression(
        definedVariables: List<Map<String, Boolean>>,
        expression: Expression
    ) {
        val usedVariables = collectVariableInExpression(expression)
        updateInspectionErrorsByExpressionVariables(definedVariables, usedVariables)
    }

    private fun updateInspectionErrorsByExpressionVariables(
        definedVariables: List<Map<String, Boolean>>,
        usedVariables: Set<VariableAccess>
    ) {
        nonInitializedVariables.addAll(
            usedVariables.asSequence().filterNot {
                for (curDepthVariables in definedVariables.asReversed()) {
                    if (curDepthVariables.containsKey(it.name)) {
                        return@filterNot curDepthVariables[it.name]!!
                    }
                }
                return@filterNot false

            }.map { Pair(it, generateNonInitializedMessage(it.name)) }
        )
    }
}