package org.jetbrains.dummy.lang.inspections

import org.jetbrains.dummy.lang.InspectionError
import org.jetbrains.dummy.lang.tree.*

enum class Type { INT, BOOLEAN, VOID, UNKNOWN }

class TypeInspections :
    DummyLangVisitor<Pair<InspectionError, Type?>, Pair<String?, List<MutableMap<String, Type>>>>() {
    private val inspections: MutableList<Pair<Element, String>> = ArrayList()
    private val functionTypes: MutableMap<String, Type> = HashMap()
    private lateinit var functionDeclarations: MutableMap<String, FunctionDeclaration>


    override fun visitElement(
        element: Element,
        data: Pair<String?, List<MutableMap<String, Type>>>
    ): Pair<InspectionError, Type?> {
        element.acceptChildren<Pair<String?, List<MutableMap<String, Type>>>>(this, Pair(null, emptyList()))
        return Pair(InspectionError(inspections), null)
    }

    override fun visitFile(
        file: File,
        data: Pair<String?, List<MutableMap<String, Type>>>
    ): Pair<InspectionError, Type?> {
        functionDeclarations = file.functions.associateBy { it.name }.toMutableMap()
        for (function in file.functions) {
            visitFunctionDeclaration(function, Pair(function.name, emptyList()))
        }
        return Pair(InspectionError(inspections), null)
    }

    override fun visitFunctionDeclaration(
        functionDeclaration: FunctionDeclaration,
        data: Pair<String?, List<MutableMap<String, Type>>>
    ): Pair<InspectionError, Type?> {
        functionTypes[functionDeclaration.name] = Type.UNKNOWN

        var result = visitBlock(
            functionDeclaration.body,
            Pair(
                functionDeclaration.name,
                listOf(functionDeclaration.parameters.map { it to Type.UNKNOWN }.toMap().toMutableMap())
            )
        )
        if (result.second != null) {
            functionTypes[functionDeclaration.name] = result.second!!
        } else {
            val curFunctionType = functionTypes[functionDeclaration.name]!!
            if (functionTypes[functionDeclaration.name]!! == Type.UNKNOWN) {
                functionTypes[functionDeclaration.name] = Type.VOID
            } else {
                addInspectionDifferentReturnType(
                    functionDeclaration,
                    functionDeclaration.name,
                    curFunctionType,
                    Type.VOID
                )
            }
            result = Pair(result.first, Type.VOID)
        }
        return result
    }

    override fun visitBlock(
        block: Block,
        data: Pair<String?, List<MutableMap<String, Type>>>
    ): Pair<InspectionError, Type?> {
        val functionName = data.first ?: throw IllegalArgumentException("function name in data.first must be not null")
        val context = data.second.plus(HashMap())
        for (statement in block.statements) {
            when (statement) {
                is Assignment -> processAssigment(statement, context)

                is VariableDeclaration -> {
                    if (statement.initializer == null) {
                        context.last()[statement.name] = Type.UNKNOWN
                    } else {
                        context.last()[statement.name] =
                            deduceExpressionType(statement.initializer, context) ?: Type.UNKNOWN
                    }
                }

                is ReturnStatement -> {
                    return processReturn(statement, context, functionName, block)
                }

                is IfStatement -> {
                    val result = processIf(statement, context, functionName, block)
                    if (result != null) {
                        return Pair(InspectionError(inspections), result)
                    }
                }
            }
        }
        return Pair(InspectionError(inspections), null)
    }

    private fun processIf(
        ifStatement: IfStatement,
        context: List<MutableMap<String, Type>>,
        functionName: String,
        block: Block
    ): Type? {
        val conditionType = deduceExpressionType(ifStatement.condition, context)
        if (conditionType != Type.BOOLEAN) {
            inspections.add(Pair(ifStatement.condition, "condition has non boolean type - $conditionType"))
        }
        val ifType = visitBlock(ifStatement.thenBlock, Pair(functionName, context))
        if (ifStatement.elseBlock != null) {
            val elseType = visitBlock(ifStatement.elseBlock, Pair(functionName, context))
            if (ifType.second != null && elseType.second != null) {
                inspectCodeAfterReturn(ifStatement, block)
                return if (ifType.second == elseType.second) {
                    ifType.second
                } else {
                    null
                }
            }
        }
        return null
    }

    private fun processAssigment(assignment: Assignment, context: List<MutableMap<String, Type>>) {
        val expressionType = deduceExpressionType(assignment.rhs, context) ?: return
        for (level in context.size - 1 downTo 0) {
            val contextLevel = context[level]
            if (contextLevel.containsKey(assignment.variable)) {
                val variableType = contextLevel[assignment.variable]!!
                if (variableType == Type.UNKNOWN) {
                    contextLevel[assignment.variable] = expressionType
                } else if (variableType != expressionType) {
                    inspections.add(
                        Pair(
                            assignment,
                            "for variable '${assignment.variable}', which has type $variableType uncorrected assigment to type $expressionType"
                        )
                    )
                }
                return
            }
        }
    }


    private fun processReturn(
        statement: ReturnStatement,
        context: List<MutableMap<String, Type>>,
        functionName: String,
        block: Block
    ): Pair<InspectionError, Type?> {
        val resultType = if (statement.result == null) {
            Type.VOID
        } else {
            deduceExpressionType(statement.result, context)
        }
        inspectReturn(statement, resultType, functionName)
        inspectCodeAfterReturn(statement, block)
        return Pair(InspectionError(inspections), resultType)
    }

    private fun deduceExpressionType(expression: Expression, variableContext: List<MutableMap<String, Type>>): Type? =
        when (expression) {
            is IntConst -> Type.INT
            is BooleanConst -> Type.BOOLEAN
            is VariableAccess -> {
                val variable = findVariableInContext(expression.name, variableContext)
                if (variable != null && variable.second != Type.UNKNOWN) {
                    variable.second
                } else {
                    inspections.add(Pair(expression, "can't deduce type for variable '${expression.name}'"))
                    null
                }
            }
            is FunctionCall -> {
                if (functionTypes.containsKey(expression.function)) {
                    if (functionTypes[expression.function] == Type.UNKNOWN) {
                        inspections.add(Pair(expression, "can't deduce type for function '${expression.function}'"))
                    }
                    functionTypes[expression.function]!!

                } else {
                    visitFunctionDeclaration(
                        functionDeclarations[expression.function]!!,
                        Pair(null, variableContext)
                    ).second
                }
            }
        }

    private fun findVariableInContext(name: String, context: List<Map<String, Type>>): Pair<String, Type>? {
        for (curContextLevel in context.asReversed()) {
            if (curContextLevel.containsKey(name)) {
                return Pair(name, curContextLevel[name]!!)
            }
        }
        return null
    }

    private fun inspectReturn(statement: Statement, returnedPartType: Type?, functionName: String) {
        if (returnedPartType == null) return
        val functionType = functionTypes[functionName]!!
        if (functionType != Type.UNKNOWN) {
            if (functionType != returnedPartType) {
                addInspectionDifferentReturnType(statement, functionName, functionType, returnedPartType)
            }
        } else {
            functionTypes[functionName] = returnedPartType
        }
    }

    private fun inspectCodeAfterReturn(statement: Statement, block: Block) {
        if (statement !== block.statements.last()) {
            inspections.add(Pair(statement, "code after return statement"))
        }
    }

    private fun addInspectionDifferentReturnType(
        element: Element,
        functionName: String,
        firstType: Type,
        secondType: Type
    ) {
        inspections.add(
            element to
                    "function '$functionName' contains different return statements: return type $firstType and type $secondType"
        )
    }
}
