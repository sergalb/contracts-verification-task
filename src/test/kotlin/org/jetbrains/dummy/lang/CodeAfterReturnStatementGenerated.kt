package org.jetbrains.dummy.lang

import org.junit.Test

class CodeAfterReturnStatementGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testPlainReturnAndCode() {
        doTest("testData\\codeAfterReturnStatement\\plainReturnAndCode.dummy")
    }
    
    @Test
    fun testReturnInBothPartOfIf() {
        doTest("testData\\codeAfterReturnStatement\\returnInBothPartOfIf.dummy")
    }
}
