package org.jetbrains.dummy.lang

import org.junit.Test

class InitialInspectionTestsGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testBad() {
        doTest("testData\\initialInspectionTests\\bad.dummy")
    }
    
    @Test
    fun testGood() {
        doTest("testData\\initialInspectionTests\\good.dummy")
    }
    
    @Test
    fun testNestedShadowsBad() {
        doTest("testData\\initialInspectionTests\\nestedShadowsBad.dummy")
    }
    
    @Test
    fun testNestedShadowsGood() {
        doTest("testData\\initialInspectionTests\\nestedShadowsGood.dummy")
    }
    
    @Test
    fun testUnknownVariable() {
        doTest("testData\\initialInspectionTests\\unknownVariable.dummy")
    }
    
    @Test
    fun testVariableFromParameter() {
        doTest("testData\\initialInspectionTests\\variableFromParameter.dummy")
    }
}
