package org.jetbrains.dummy.lang

import org.junit.Test

class TypeInspectionTestsGenerated : AbstractDummyLanguageTest() {
    @Test
    fun testDifferentReturnTypeInIf() {
        doTest("testData\\typeInspectionTests\\differentReturnTypeInIf.dummy")
    }
    
    @Test
    fun testReturnInIfAndOutside() {
        doTest("testData\\typeInspectionTests\\returnInIfAndOutside.dummy")
    }
    
    @Test
    fun testSameTypeInIf() {
        doTest("testData\\typeInspectionTests\\sameTypeInIf.dummy")
    }
    
    @Test
    fun testTypeMismatchInAssign() {
        doTest("testData\\typeInspectionTests\\typeMismatchInAssign.dummy")
    }
    
    @Test
    fun testTypeMismathThroghFunctionCall() {
        doTest("testData\\typeInspectionTests\\typeMismathThroghFunctionCall.dummy")
    }
}
